
$(document).ready(function () {

	// menu 

	$(".wrap-icon-toggle").click(function () {
		$(".wrap-icon-toggle").toggleClass('active');
		$('body').toggleClass('overlay');
		$('nav#nav').toggleClass('active');
		$('header#header').toggleClass('active-menu');
		$('main#main').toggleClass('active-menu');
		$('footer#footer').toggleClass('active-menu');
	});
	$(document).mouseup(function (e) {
		var container = $("nav#nav, header#header");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
				$('.wrap-icon-toggle').removeClass('active');
				$('body').removeClass('overlay');
				$('nav#nav').removeClass('active');
				$('header#header').removeClass('active-menu');
				$('main#main').removeClass('active-menu');
				$('footer#footer').removeClass('active-menu');
		}
	});

	function resize(){
		var win = $(window).width();

		if(win < 1025){
			$('.icon-arrow-menu').click(function(){
				$(this).toggleClass('active');
				$(this).siblings('.menu-child').slideToggle();
			})
		};
		

	}
	resize();
	$(window).on('resize', function(){
        resize();
    });

	var win = $(window).width();
	if(win < 1080){
		if($('div').hasClass('wrap-box-list-orther')){
			$('.wrap-box-list-orther>.row').append(`<div class="wrap-icon-control"></div>`);
		}
	};
	if(win < 769){
		
		$(".banner-item.banner-angle .row").append(`<div class="wrap-icon-control"></div>`)
	};
	if(win < 577){
		if($('.wrap-multimedia .row').hasClass('wrap-list-video')){
			$('.wrap-multimedia .wrap-list-video').append(`<div class="wrap-icon-control"></div>`);
			$('.wrap-multimedia .wrap-list-video .item-box-libary .box-image').removeClass("cover-size-3");
			$('.wrap-multimedia .wrap-list-video .item-box-libary .box-image').addClass("cover-size-4");
		}
		if($('#wrap-banner-list-two .row').hasClass('wrap-list-blog-item')){
			$('#wrap-banner-list-two .row.wrap-list-blog-item .row').append(`<div class="wrap-icon-control"></div>`);
		}
		if($('.banner-community div').hasClass('row')){
			$('.banner-community .row').append(`<div class="wrap-icon-control"></div>`);
		};
		$(".box-item-footer").append(`<div class="icon-dropdown"></div>`)
	};

	
	// 
	$(document).on('click','.icon-dropdown',function(){
		if($(this).parent('.box-item-footer').hasClass('active')){
			$('.box-item-footer').removeClass("active");
			$('.icon-dropdown').removeClass("active");
		} else{
			$('.box-item-footer').removeClass("active");
			$('.icon-dropdown').removeClass("active");
			$(this).toggleClass("active");
			$(this).parent('.box-item-footer').toggleClass("active");

		}
	})

	// cover size text 

	var count = 16;
	$("#minus").click(function () {
		if (count > 10) {
			count--;
			$('.wrap-blog-detail-main *').css('font-size', +count + 'px');
		}
	});
	$("#plus").click(function () {
		if (count < 25) {
			count++;
			$('.wrap-blog-detail-main *').css('font-size', +count + 'px');
		}
	});
	$('.cover-size span:first-child').click(function () {
		$('.wrap-blog-detail-main *').css('font-size', '16px');
	});


	// get date now 
	function getTime() {
		var objToday = new Date(),
			weekday = new Array('Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'),
			dayOfWeek = weekday[objToday.getDay()],
			dayOfMonth = today + (objToday.getDate() < 10) ? '0' + objToday.getDate() : objToday.getDate(),
			months = new Array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'),
			curMonth = months[objToday.getMonth()],
			curYear = objToday.getFullYear();
		var today = dayOfWeek + ", ngày " + dayOfMonth + " tháng " + curMonth + " năm " + curYear;
		if($("#date-now").hasClass('date')){
			document.getElementById('date-now').innerHTML = today;
		}
	}
	setInterval(getTime, 1000);





});

// slide 
$('#slide-banner-main').owlCarousel({
	nav: true,
	autoplay: true,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 0,
	responsive: {
		0: {
			items: 1,
			loop: ($('#slide-banner-main .item').length > 1) ? true : false,
		},
	}
});


function resizeImage() {
	let arrClass = [
		{ class: 'cover-size-1', number: (432/770) },
		{ class: 'cover-size-2', number: (51/90) },
		{ class: 'cover-size-3', number: (373/585) },
		{ class: 'cover-size-4', number: (156/277) },
		{ class: 'cover-size-5', number: (401/278) },
		{ class: 'cover-size-6', number: (475/844) },
		{ class: 'cover-size-7', number: (79/140) },
		{ class: 'cover-size-8', number: (350/600) },
		{ class: 'cover-size-9', number: (185/277) },
		{ class: 'cover-size-10', number: (391/586) },
		{ class: 'cover-size-11', number: (195/292) },
		{ class: 'cover-size-12', number: (253/380) },
		{ class: 'cover-size-13', number: (214/380) },
		{ class: 'cover-size-14', number: (506/900) },
		
	];
	for (let i = 0; i < arrClass.length; i++) {
		let width = $("." + arrClass[i]['class']).width();
		$("." + arrClass[i]['class']).css('height', width * arrClass[i]['number'] + 'px');
		// console.log(width);
		// console.log(arrClass);
		// console.log(width*arrClass[i]['number']);
	}
}
resizeImage();
$(window).on('resize', function () {
	resizeImage();
});

// active youtube
function activeUrlVideo(attr) {
	var id_video = $(attr).data('url');
	var symbol = $("#video-main")[0].src;
	$("#video-main")[0].src = 'https://www.youtube.com/embed/' + id_video + '?autoplay=1';
}
function closeVideo() {
	var symbol = $("#video-main")[0].src;
	var arr = symbol.split('embed/');
	$("#video-main")[0].src = arr[0] + 'embed/';
}
$(".active-video").click(function () {
	activeUrlVideo(this);
});
$(".close").click(function () {
	closeVideo();
});
$(document).mouseup(function (e) {
	var container = $("#video-youtube .wrap-video-youtube");
	if (!container.is(e.target) &&
		container.has(e.target).length === 0) {
		if ($('.modal').hasClass('show')) {
			closeVideo();
		}
	}
});

// choose image
function readURL(input) {
	if (input.files && input.files[0]) {
		var url = URL.createObjectURL(event.target.files[0]);
		$(input).parent().find('span.invalid-feedback').text('');
		$(input).parent().find('div.preview').show();
		$(input).parent().find('div.preview').attr("style", "background: #eef0f8 url('" + url + "') no-repeat top center; background-size: cover; display: block; background-position: center");
		$(input).parent().find('div.fill').addClass('active');
		$(input).parent().find('.b-drop').addClass('active');
	}
}
$("#file").change(function() {
	readURL(this);
  });


//   scroll header 
$(window).on('scroll', function () {
	if ($(window).scrollTop()) {
		$('#header').addClass('active');
	} else {
		$('#header').removeClass('active')
	};
});
